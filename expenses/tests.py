from django.test import TestCase
from .models import TestModel
# Create your tests here.


class AccountTestCase (TestCase):
    def setUp(self):
        TestModel.objects.create(name="Manzi", age=22)

    def test_if_is_exist(self):
        manzi = TestModel.objects.get(name="Manzi")
        self.assertEqual(manzi.age, 22)


class SecondTestCase (TestCase):
    def setUp(self):
        TestModel.objects.create(name="Brian", age=22)

    def test_if_is_exist(self):
        manzi = TestModel.objects.get(name="Brian")
        self.assertEqual(manzi.name, "Brian")
