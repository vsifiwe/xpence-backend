# XPENCE

This Django application is an expense and income tracker that allows a user to create an account, expense and income categories and record transactions(Expense or Transaction).

## Installation

### Requirements

##### Backend

- Python 3

### Live Testing

- The API is hosted on Heroku [on this link](https://xpence-django.herokuapp.com/)

### Local Machine testing

- Clone [this](https://gitlab.com/vsifiwe/xpence-backend) project from GitLab to your local machine
- In the project directory, activate the environment using `source Scripts/activate` on Linux
- Install required dependencies using `pip install -r requirements.txt`
- After that is done, create migrations using `python manage.py makemigrations`
- Migrate using `python manage.py migrate`
- Every thing should be done, now you can run the server using `python manage.py runserver`
- Open [http://localhost:8000](http://localhost:8000) to test it.

### Swagger Documentation

The swagger documentation is hosted [on Heroku](https://xpence-django.herokuapp.com/) or locally on port 8000

### Screenshots

![Dashboard](https://github.com/vsifiwe/xpence/blob/master/screenshots/dashboard.png?raw=true)
![Login](https://github.com/vsifiwe/xpence/blob/master/screenshots/login.png?raw=true)
![Register](https://github.com/vsifiwe/xpence/blob/master/screenshots/register.png?raw=true)
![New Account](https://github.com/vsifiwe/xpence/blob/master/screenshots/new_account.png?raw=true)
![New Expense](https://github.com/vsifiwe/xpence/blob/master/screenshots/new_expense.png?raw=true)
![New Income](https://github.com/vsifiwe/xpence/blob/master/screenshots/new_income.png?raw=true)
